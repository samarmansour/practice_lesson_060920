﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Dictionary
{
    class MyDictionaryIntToString
    {
        public List<int> keys { get; set; }
        public List<string> values { get; set; }

        public MyDictionaryIntToString()
        {
            keys = new List<int>();
            values = new List<string>();
        }


        public void Add(int k, string v)
        {
            if (keys.Contains(k))
            {
                throw new Exception("Key already exist");
            }
            else
            {
                keys.Add(k);
                values.Add(v);
            }
            
        }

        public bool TryAdd(int key, string value)
        {
            //return (keys.Contains(key) && values.Contains(value));
            if (keys.Contains(key) && values.Contains(value))
            {
                return true;
            }
            return false;
        }

        public string TryGet(int key)
        {
            if (keys.Contains(key))
            {
                int index = keys.IndexOf(key);
                return values[index];
            }
            return null;
        }

        public void AddOrOverWrite(int key, string value)
        {
            if (keys.Contains(key))
            {
                int index = keys.IndexOf(key);
                values[index] = value;
            }
            else
            {
                keys.Add(key);
                values.Add(value);
            }
        }
        public string Get(int key)
        {
            if (keys.Contains(key))
            {
                int index = keys.IndexOf(key);
                return values[index];
            }
            throw new Exception("key not exist");
        }

        public void Clear()
        {
            keys.Clear();
            values.Clear();
        }

        public bool ContainsKey(int key)
        {
            return keys.Contains(key);
        }

        public bool ContainsValue(string value)
        {
            return values.Contains(value);
        }

        public int CountItemWithThisValue(string value)
        {
            int count = 0;
            foreach (string v in values)
            {
                if (value == v)
                {
                    count++;
                }
            }
            return count;
        }

        public List<int> GetKeys()
        {
            if (keys.Count != 0)
            {
                return keys;
            }
            return null;
        }

        public List<string> GetValue()
        {
            if (values.Count != 0)
            {
                return values;
            }
            return null;
        }
        public static MyDictionaryIntToString operator +(MyDictionaryIntToString me, KeyValuePair<int, string> item)
        {
            me.Add(item.Key, item.Value);
            return me;
        }

        public static MyDictionaryIntToString operator -(MyDictionaryIntToString me, int key)
        {
            if (!me.ContainsKey(key))
                return me;
            MyDictionaryIntToString result = new MyDictionaryIntToString();
            foreach (int k in me.keys)
            {
                result.Add(k, me.Get(k));
            }
            int index = me.keys.IndexOf(key);
            me.keys.RemoveAt(index);
            me.values.RemoveAt(index);

            return me;
        }

        public static MyDictionaryIntToString operator +(MyDictionaryIntToString mydict1, MyDictionaryIntToString mydict2)
        {
            MyDictionaryIntToString result = new MyDictionaryIntToString();
            foreach (int key in mydict1.keys)
            {
                result.Add(key, mydict1.Get(key));
            }
            foreach (int key in mydict2.keys)
            {
                result.TryAdd(key, mydict2.Get(key));
            }
            return result;
        }

        public static MyDictionaryIntToString operator -(MyDictionaryIntToString mydict1, MyDictionaryIntToString mydict2)
        {
            MyDictionaryIntToString result = new MyDictionaryIntToString();
            foreach (int k in mydict1.keys)
            {
                result.Add(k, mydict1.Get(k));
            }
            foreach (int k in mydict2.keys)
            {
                result = result - k;
            }
            return result;
        }

        public override string ToString()
        {
            string print = $"Dictionary Length: {keys.Count()}\n";
            if (keys.Count == values.Count)
            {
                for (int i = 0; i < keys.Count; i++)
                {
                    print += $"Key: {keys[i]} Value: {values[i]}\n";
                }
            }
            return print;
        }


        public static bool operator ==(MyDictionaryIntToString mydict1, MyDictionaryIntToString mydict2)
        {
            if (!(mydict1.keys.Count !=  mydict2.keys.Count)) 
            {
                return false;
            }
            foreach (int key in mydict1.keys)
            {
                string value = mydict1.Get(key);
                if (!mydict2.ContainsKey(key))
                {
                    return false;
                }
                string value2 = mydict2.Get(key);
                if (value != value2)
                {
                    return false;
                }
            }
            return true;

        }
        public static bool operator !=(MyDictionaryIntToString mydict1, MyDictionaryIntToString mydict2)
        {
            return !(mydict1 == mydict2);
        }

        public static bool operator >(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is bigger than number of items in mydic2
            // implement
            return mydic1.keys.Count > mydic2.keys.Count && mydic1.values.Count > mydic2.values.Count;
        }
        public static bool operator <(MyDictionaryIntToString mydic1, MyDictionaryIntToString mydic2)
        {
            // will return true if numebr of items in mydic1 is smaller than number of items in mydic2
            // implement 
            return mydic1.keys.Count < mydic2.keys.Count && mydic1.values.Count < mydic2.values.Count;
        }
        public override bool Equals(object obj)
        {
            MyDictionaryIntToString dict = obj as MyDictionaryIntToString;
            return this == dict;

        }

    }
}
